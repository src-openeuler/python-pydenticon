%global modname pydenticon

Name:           python-%{modname}
Version:        0.3.1
Release:        2
Summary:        Library for generating identicons

License:        BSD-3-Clause
URL:            https://github.com/azaghal/pydenticon
Source0:        %{modname}-%{version}.tar.gz

BuildArch:      noarch

%global _description \
Pydenticon is a small utility library that can be used for deterministically\
enerating identicons based on the hash of provided data.

%description %{_description}

%package -n python3-%{modname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{modname}}
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

BuildRequires:  python3dist(mock)
BuildRequires:  python3dist(pillow)

%description -n python3-%{modname} %{_description}

Pydenticon is a small utility library that can be used for deterministically\
enerating identicons based on the hash of provided data.

%prep
%autosetup -n %{modname}-%{version}

%build
%py3_build

%install
%py3_install

%check
%{__python3} setup.py test

%files -n python3-%{modname}
%license LICENSE
%doc README.rst
%{python3_sitelib}/%{modname}-*.egg-info/
%{python3_sitelib}/%{modname}/

%changelog
* Wed May 11 2022 xigaoxinyan <xigaoxinyan@h-partners.com> - 0.3.1-2
- License compliance rectification

* Wed Jul 7 2021 Lianguo Wang <wanglianguo@kylinos.cn> - 0.3.1-1
- Initial package for openEuler, version is 0.3.1
